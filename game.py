from random import randint

name = input("Hi! What is your name?")
print("Hello", name, "Nice to meet you!")
print("I'm going to guess your birthday")

for guess_number in range(1, 6):
    birthday_month = randint(1, 12)
    birthday_year = randint(1924, 2004)

    print("Guess", guess_number, "were you born in",
        birthday_month, "/", birthday_year, "?")

    response = input("yes or no?")
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have elsewhere to be!")
    else:
        print("Drat! Lemme try again!")
